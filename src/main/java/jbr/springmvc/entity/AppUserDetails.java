package jbr.springmvc.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_details")

public class AppUserDetails implements Serializable{
	@Id
	@GenericGenerator(name = "user_auto", strategy = "increment")
	@GeneratedValue(generator = "user_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "city")
	private String city;

	@Column(name = "contact_number")
	private Long contactNumber;
	
	@Column(name = "country")
	private String country;
	
	@Column(name="password")
	private String password;
	
	@Column(name = "pin_code")
	private String pinCode;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "o_id")
	private AppOwnerDetails appOwnerDetails;
	
	public AppOwnerDetails getAppOwnerDetails() {
		return appOwnerDetails;
	}

	public void setAppOwnerDetails(AppOwnerDetails appOwnerDetails) {
		this.appOwnerDetails = appOwnerDetails;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	@Override
	public String toString() {
		return "AppUserDetails [id=" + id + ", name=" + name + ", email=" + email + ", city=" + city
				+ ", contactNumber=" + contactNumber + ", country=" + country + ", password=" + password + ", pinCode="
				+ pinCode + ", appOwnerDetails=" + appOwnerDetails + "]";
	}

	
	
	

}
