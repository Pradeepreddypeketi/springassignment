package jbr.springmvc.entity;

import java.io.Serializable;

public class VerifyOwnerDetails implements Serializable {
	private String email;
	private String password;
	
	public VerifyOwnerDetails() {
		System.out.println(this.getClass().getSimpleName()+" object created");
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "VerifyOwnerDetails [email=" + email + ", password=" + password + "]";
	}
	
	
}
