package jbr.springmvc.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jbr.springmvc.entity.AppOwnerDetails;
import jbr.springmvc.entity.AppUserDetails;
import jbr.springmvc.entity.EditUserDetails;
import jbr.springmvc.entity.VerifyOwnerDetails;

@Repository
public class AppDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public AppDAO() {
		System.out.println(this.getClass().getSimpleName()+" object created");
	}
	
	public void saveOwnerDetails(AppOwnerDetails appOwnerDetails) {
		Session session = sessionFactory.openSession();
		try {
		Transaction beginTransaction = session.beginTransaction();
		session.save(appOwnerDetails);
		beginTransaction.commit();
		}
		catch(Exception exception) {
			
		}
		finally {
		session.close();
		}
	}
	public AppOwnerDetails verifyOwnerDetails(VerifyOwnerDetails verifyOwnerDetails) {
		Session session = sessionFactory.openSession();
		String hql="from AppOwnerDetails where email=:email and password=:password";
		Query query = session.createQuery(hql);
		query.setParameter("email", verifyOwnerDetails.getEmail());
		query.setParameter("password", verifyOwnerDetails.getPassword());
		AppOwnerDetails result = (AppOwnerDetails) query.uniqueResult();
		return result;
	}
	
	public void saveUserDetails(AppUserDetails appUserDetails) {
		Session session = sessionFactory.openSession();
		try {
		Transaction beginTransaction = session.beginTransaction();
		session.save(appUserDetails);
		beginTransaction.commit();
		}
		catch(Exception exception) {
			
		}
		finally {
		session.close();
		}
	}
	public List<AppUserDetails> getAppDetailsByUserId(Long id) {
		Session session = sessionFactory.openSession();
		String hql =" from AppUserDetails where o_id=:i";
		Query query = session.createQuery(hql);
		query.setParameter("i", id);
		List<AppUserDetails> list=query.list();
		return list;
	}
	
	public AppUserDetails getUser(long id) {
		Session session = sessionFactory.openSession();
		String hql="from AppUserDetails where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id",id);
		
		AppUserDetails result = (AppUserDetails) query.uniqueResult();
		return result;
	}
	
	public void updateUser(EditUserDetails editUserDetails) {
		Session session = sessionFactory.openSession();
		System.out.println(editUserDetails+" in appDAO");
		String hql="update AppUserDetails set name=:username, email=:email, password=:password, contact_number=:contact, city=:city, country=:country, pin_code=:pinCode where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("username", editUserDetails.getName());
		query.setParameter("email", editUserDetails.getEmail());
		query.setParameter("password", editUserDetails.getPassword());
		query.setParameter("contact", editUserDetails.getContactNumber());
		query.setParameter("city", editUserDetails.getCity());
		query.setParameter("country", editUserDetails.getCountry());
		query.setParameter("pinCode", editUserDetails.getPinCode());
		query.setParameter("id", editUserDetails.getId());
		int update = query.executeUpdate();
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		System.out.println(update+" value");
	}
	
	public void deleteUser(long id) {
		Session session = sessionFactory.openSession();
		System.out.println(id+" in deleteUser()");
		String hql="delete from AppUserDetails where id=:id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		int value = query.executeUpdate();
		Transaction transaction = session.beginTransaction();
		transaction.commit();
		System.out.println(value+" value");
		
	}
}
