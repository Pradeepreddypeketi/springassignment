package jbr.springmvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import jbr.springmvc.entity.AppOwnerDetails;
import jbr.springmvc.entity.AppUserDetails;
import jbr.springmvc.entity.EditUserDetails;
import jbr.springmvc.entity.VerifyOwnerDetails;
import jbr.springmvc.service.AppService;

@Controller
@RequestMapping("/")
public class AppController {

@Autowired
private AppService appService;


public AppController() {
	System.out.println(this.getClass().getSimpleName()+" object created");
}
  @RequestMapping("/saveOwnerDetails")
  public ModelAndView saveOwnerDetails(AppOwnerDetails appOwnerDetails) {
	  System.out.println(appOwnerDetails);
	  appService.saveOwnerDetails(appOwnerDetails);
	  return new ModelAndView("ownerlogin.jsp","message","successfully registered");
  }
  
  @RequestMapping("/verifyOwnerDetails")
  public ModelAndView verifyOwnerDetails(VerifyOwnerDetails verifyOwnerDetails, HttpServletRequest request) {
	  System.out.println(verifyOwnerDetails);
	  AppOwnerDetails ownerDetails = appService.verifyOwnerDetails(verifyOwnerDetails);
	  if(ownerDetails!=null) {
		  HttpSession session = request.getSession();
		  session.setAttribute("owner", ownerDetails);
			List<AppUserDetails> list = appService.getAppdetailsByUserId(ownerDetails.getId());
			list.forEach(a->{
				int i=0;
				System.out.println(i++ +" "+a);});
			return new ModelAndView("home.jsp","list",list);

	  }
	  return new ModelAndView("ownerlogin.jsp","message","invalid credentials");
  }
  
  @RequestMapping("/saveUserDetails")
  public ModelAndView saveUserDetails(AppUserDetails appUserDetails, HttpServletRequest request) {
	  System.out.println(appUserDetails);
	  //System.out.println(id);
	  appService.saveUserDetails(appUserDetails);
	  return editUser(request);
  }

  public ModelAndView viewData(HttpServletRequest request) {
	  AppOwnerDetails appOwnerDetails = (AppOwnerDetails) request.getSession().getAttribute("owner");
	  List<AppUserDetails> list = appService.getAppdetailsByUserId(appOwnerDetails.getId());
	  return new ModelAndView("home.jsp","list",list);
  }
  @RequestMapping("/editUser")
  public ModelAndView editUser(HttpServletRequest request) {
	  long id=Long.parseLong(request.getParameter("id"));
	  AppUserDetails user = appService.getUser(id);
	  System.out.println(user+" in editUser()");
	  System.out.println("owner id :: "+user.getAppOwnerDetails().getId());
	  return new ModelAndView("edituserdata.jsp","user",user);
  }
  
  @RequestMapping("/editUserDetails")
  public ModelAndView editUserDetails(EditUserDetails editUserDetails, HttpServletRequest request) {
	  System.out.println("editUser()"+editUserDetails);
	  appService.updateUser(editUserDetails);
	  return viewData(request);
  }
  
  @RequestMapping("/deleteUser")
  public ModelAndView deleteUser(HttpServletRequest request) {
	  long id= Long.parseLong(request.getParameter("id"));
	  appService.deleteUser(id);
	  return viewData(request);
  }
}