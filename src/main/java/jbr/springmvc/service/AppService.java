package jbr.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jbr.springmvc.DAO.AppDAO;
import jbr.springmvc.entity.AppOwnerDetails;
import jbr.springmvc.entity.AppUserDetails;
import jbr.springmvc.entity.EditUserDetails;
import jbr.springmvc.entity.VerifyOwnerDetails;

@Service
public class AppService {
	@Autowired
	private AppDAO appDAO;
	
	public AppService() {
		System.out.println(this.getClass().getSimpleName()+" object created");
	}
	
	public void saveOwnerDetails(AppOwnerDetails appOwnerDetails) {
		appDAO.saveOwnerDetails(appOwnerDetails);
	}
	
	public AppOwnerDetails verifyOwnerDetails(VerifyOwnerDetails verifyOwnerDetails) {
		return appDAO.verifyOwnerDetails(verifyOwnerDetails);
	}
	
	public void saveUserDetails(AppUserDetails appUserDetails) {
		appDAO.saveUserDetails(appUserDetails);
	}
	public List<AppUserDetails> getAppdetailsByUserId(Long id) {
		return appDAO.getAppDetailsByUserId(id);
	}
	
	public AppUserDetails getUser(long id) {
		return appDAO.getUser(id);
	}

	public void updateUser(EditUserDetails editUserDetails) {
		 appDAO.updateUser(editUserDetails);
	}
	public void deleteUser(long id) {
		appDAO.deleteUser(id);
	}
}
