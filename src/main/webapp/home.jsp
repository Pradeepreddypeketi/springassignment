<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a href="userdetails.jsp"><h3>CREATE USER</h3></a>
<br><br>
<table border="1">
	<thead>
		<tr>
			<th>SI No.</th>
			<th>Name</th>
			<th>Email</th>
			<th>City</th>
			<th>contact</th>
			<th>country</th>
			<th>pin-code</th>
			<th colspan="2">action</th>
		</tr>
	</thead>
	<tbody>
 		<c:forEach items="${list}" var="app">
 			<tr>
 				<td>${app.id}</td>
 				<td>${app.name}</td>
 				<td>${app.email}</td>
 				<td>${app.city}</td>
 				<td>${app.contactNumber}</td>
 				<td>${app.country}</td>
 				<td>${app.pinCode}</td>
 				<td><a href="editUser?id=${app.id}">Edit</a>
 				<td><a href="deleteUser?id=${app.id}">Delete</a>
 			</tr>
 		</c:forEach>
 		</tbody>
</table>
</body>
</html>