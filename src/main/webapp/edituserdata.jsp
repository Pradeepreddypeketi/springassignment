<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h3>EDIT USER DETAILS</h3>
	<form action="editUserDetails" method="post">
		<div>
			<input type="hidden" name="id" value=${user.id }>
		</div>
		<div>
			<label>Name</label> <input type="text" name="name" value=${user.name }>
		</div>
		<div>
			<label>Email</label> <input type="email" name="email" value=${user.email }>
		</div>

		<div>
			<label>Contact Number</label> <input type="tel" name="contactNumber" value=${user.contactNumber }>
		</div>
		<div>
			<label>Password</label> <input type="password" name="password" value=${user.password }>
		</div>
		<div>
			<label>City</label> <input type="text" name="city" value=${user.city }>
		</div>
		<div>
			<label>Country</label> <input type="text" name="country" value=${user.country }>
		</div>
		<div>
			<label>Pin code</label> <input type="text" name="pinCode" value=${user.pinCode }>
		</div>

		<div>
			<input type="submit" value="Submit">
		</div>

	</form>

</body>
</html>